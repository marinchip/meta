
INTRODUCTION

This repo contains the META 3.5 compiler generator. It is the actual 1981
source code which all survived. The META family of "compiler compilers" are
programs that take a BNF like language description and generate a
recursive descent parser for that language.

BACKGROUND

In the original META 2 paper, it outputs assembly language for a virtual
machine with 12 specialised instructions. Implementing an interpreter for
that VM is just a few hundred lines of code and easily done by hand. An
implementation in C (together with an integrated VM assembler) can be found
here: https://zenodo.org/records/31610

A full tutorial on META 2 can be found here:
https://www.bayfronttechnologies.com/mc_tutorial.html

The original META 3 paper took the above idea a step further and replaced
the VM by actual assembler code for the IBM7094, using a mix of basic machine
instructions and a set of calls to supporting library routines. This seems
to have been the basis for Marinchip's META.

META AT MARINCHIP

According to John Walker, META was the first program done for the M9900
using the native assembler tool chain. It took META 3 and ported it from
the IBM7094 to the M9900. From there it played a role similar to "TMG" and
later YACC on Unix.

It would seem that Mike Riddle became the maintainer for this META 3,
releasing various updates culminating in META 3.5 in 1981. Mike used the
META compiler to build the SPL and QBasic compilers.

BUILDING

The surviving META 3.5 disk contains the full source for it. This was
more or less neccessary to make it usuable for building compilers with
the product: without providing the source for the support library it
would not have been much of a time saver.

The "make.sh" script shows how to build META from source. Note that - like
spascal - it is a circular build: you need meta to build meta (although
one could have shipped a generated meta.asm file to allow a full bootstrap.)
