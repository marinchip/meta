#!/bin/sh

#
# assumes MDEX emulator is located in ../emu
#

# compile "meta.met" to "src/meta.asm"
../emu/host meta =meta.met,src/meta.asm

# assemble meta files, place in reloc
src_files="buffers mlclass mlopen mltokens conio mlclose mlout \
	   mltst free mlerrors mlscan mlutil mlcall mlsymtb parmn \
	   meta"

cd src
for i in $src_files
do
	echo reloc/$i.rel=src/$i.asm
	../../emu/host 1/asm ../reloc/$i.rel=$i.asm
done

# link relocatables into new "meta" executable
cd ..
../emu/host 1/link meta=@meta.lnk

